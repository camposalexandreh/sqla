package br.com.jeanfbs.sqla.controller;


import br.com.jeanfbs.sqla.components.NumberField;
import br.com.jeanfbs.sqla.components.alerts.ExceptionAlert;
import br.com.jeanfbs.sqla.context.ApplicationContext;
import br.com.jeanfbs.sqla.db.DataBaseFactory;
import br.com.jeanfbs.sqla.db.DriverTypeEnum;
import br.com.jeanfbs.sqla.db.sqlserver.SQLConnectionAttributes;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.Connection;

@Component
public class LoginController {


    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @FXML
    private RadioButton oracleRadioBtn;

    @FXML
    private ToggleGroup instance_group;

    @FXML
    private TextField hostTextField;

    @FXML
    private TextField usernameTextField;


    @FXML
    private TextField passwordTextField;


    @FXML
    private NumberField portTextField;

    @FXML
    private TextField dataBaseTextField;

    @FXML
    private Label message;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private ListView<SQLConnectionAttributes> listViewHistoric;

    private ObservableList<SQLConnectionAttributes> connectionsHistoric;


    @Autowired
    private DataBaseFactory dataBaseFactory;

    private ExceptionAlert exceptionAlert;


    @FXML
    public void initialize() {

        connectionsHistoric = FXCollections.observableArrayList();

        hostTextField.setText("localhost");
        usernameTextField.setText("sa");
        passwordTextField.setText("Mudar123");
        portTextField.setText("1433");
        dataBaseTextField.setText("sqla");
        oracleRadioBtn.setSelected(true);

        Platform.runLater(() -> exceptionAlert = new ExceptionAlert());
        listViewHistoric.setCellFactory(param -> new HistoricCell());
        listViewHistoric.setItems(connectionsHistoric);
        progressBar.setVisible(false);

    }

    @FXML
    private void connectHandlerClickEvent(ActionEvent event){

        if(!checkRequiredFields()){
            message.setText("Preencha todos os campos obrigatórios!");
            message.setVisible(true);
            message.setTextFill(Paint.valueOf("#cc0000"));
            return;
        }

        SQLConnectionAttributes attrs = new SQLConnectionAttributes(hostTextField.getText(), usernameTextField.getText(),
                passwordTextField.getText(), Integer.parseInt(portTextField.getText()), dataBaseTextField.getText());

        try {
            DriverTypeEnum driverTypeEnum = DriverTypeEnum.valueOf(instance_group.getSelectedToggle()
                    .getUserData().toString());

            Connection con = dataBaseFactory.getInstance(driverTypeEnum, attrs);
            ApplicationContext.setCurrentConnection(con);

            con.prepareCall("SELECT 1;").execute();
            message.setText("Sucesso!");
            message.setVisible(true);
            message.setTextFill(Paint.valueOf("#2d8a31"));

            long count = connectionsHistoric.stream().filter(historic -> historic.toString().equals(attrs.toString())).count();
            if(count == 0){
                connectionsHistoric.add(attrs);
            }

            KeyFrame key1 = new KeyFrame(Duration.seconds(2), ev -> {
                try {
                    loadHomeView(event);
                    closeWindow(event);
                } catch (IOException e) {
                    log.error("", e);
                }
            });

            Timeline timeline = new Timeline(key1);
            Platform.runLater(timeline::play);

            progressBar.setVisible(true);
            updateProgressBar();

        } catch (final Exception ex) {
            log.error("",ex);

            message.setText("Erro na conexão!");
            message.setVisible(true);
            message.setTextFill(Paint.valueOf("#cc0000"));

            exceptionAlert.setTitle("Erro na conexão");
            exceptionAlert.setHeaderText("Erro na conexão com o banco de dados");
            exceptionAlert.setException(ex);
            exceptionAlert.showAndWait();
        }
    }


    private void updateProgressBar(){
        final Task<Void> task = new Task<Void>() {
            final int N_ITERATIONS = 100;

            @Override
            protected Void call() throws Exception {
                for (int i = 0; i < N_ITERATIONS; i++) {
                    updateProgress(i + 1, N_ITERATIONS);
                    Thread.sleep(17);
                }
                return null;
            }
        };

        progressBar.progressProperty().bind(
                task.progressProperty()
        );

        final Thread thread = new Thread(task, "task-thread");
        thread.setDaemon(true);
        thread.start();

    }


    private void loadHomeView(ActionEvent ev) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/home.fxml"));

        Parent parent = fxmlLoader.load();
        Stage stage = new Stage();
        Rectangle2D bounds = Screen.getPrimary().getBounds();

        stage.setScene(new Scene(parent, bounds.getWidth(), bounds.getHeight()));
        stage.show();
    }

    private void closeWindow(ActionEvent ev){
        ((Node)(ev.getSource())).getScene().getWindow().hide();
    }
    @FXML
    private void cleanHandlerClickEvent(){
        hostTextField.clear();
        usernameTextField.clear();
        passwordTextField.clear();
        portTextField.clear();
        dataBaseTextField.clear();
    }

    private Boolean checkRequiredFields() {
        return StringUtils.isNotEmpty(hostTextField.getText())
                && StringUtils.isNotEmpty(usernameTextField.getText())
                && StringUtils.isNotEmpty(passwordTextField.getText())
                && StringUtils.isNotEmpty(portTextField.getText())
                && StringUtils.isNotEmpty(dataBaseTextField.getText());
    }


    class HistoricCell extends ListCell<SQLConnectionAttributes> {


        @FXML
        private Label host;

        @FXML
        private Label user;

        @FXML
        private Label password;

        @FXML
        private Label database;

        @FXML
        private HBox rowPane;

        private FXMLLoader mLLoader;


        @Override
        protected void updateItem(SQLConnectionAttributes attrs, boolean empty) {
            super.updateItem(attrs, empty);

            if(empty || attrs == null) {

                setText(null);
                setGraphic(null);

            } else {
                if (mLLoader == null) {
                    mLLoader = new FXMLLoader(getClass().getResource("/views/cells/historicCell.fxml"));
                    mLLoader.setController(this);

                    try {
                        mLLoader.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                host.setText(String.format("%s:%s", attrs.getHostName(), attrs.getPort()));

                user.setText(attrs.getUsername());
                password.setText(attrs.getPassword());
                database.setText(attrs.getDataBase());

                setText(null);
                setGraphic(rowPane);
            }
        }
    }
}
