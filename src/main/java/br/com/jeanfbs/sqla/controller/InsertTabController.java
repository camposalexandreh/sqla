package br.com.jeanfbs.sqla.controller;

import br.com.jeanfbs.sqla.enums.QueryType;
import br.com.jeanfbs.sqla.vo.ColumnValueData;
import br.com.jeanfbs.sqla.vo.Query;
import br.com.jeanfbs.sqla.vo.Table;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableStringValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class InsertTabController {


    private TableView<ColumnValueData> valuesTableView;

    private ObservableList<ColumnValueData> valuesObservable;

    private TableColumn<ColumnValueData, String> colColuna;

    private TableColumn<ColumnValueData, String> colTipoDado;

    private TableColumn<ColumnValueData, String> colValor;

    private Button btnSave;

    private Table table;

    private Label insertMessageLabel;

    private ListView<Query> queriesListView;

    public InsertTabController(TableView<ColumnValueData> valuesTableView,
                               Label insertMessageLabel,
                               Button btnSave) {

        this.valuesTableView = valuesTableView;
        this.valuesObservable = valuesTableView.getItems();
        this.colColuna = (TableColumn<ColumnValueData, String>)valuesTableView.getColumns().get(0);
        this.colTipoDado = (TableColumn<ColumnValueData, String>)valuesTableView.getColumns().get(1);;
        this.colValor = (TableColumn<ColumnValueData, String>)valuesTableView.getColumns().get(2);;
        this.btnSave = btnSave;
        this.insertMessageLabel = insertMessageLabel;
        this.initColumns();
        this.editableColumns();

        btnSave.setOnAction(this::handleBtnSave);
        btnSave.setDisable(true);
    }


    private void initColumns(){
        colColuna.setCellFactory(param -> new CustomColumnCell());
        colColuna.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getColumn() + "#" + param.getValue().getRequired()));
        colTipoDado.setCellValueFactory(new PropertyValueFactory<>("type"));
        colValor.setCellValueFactory(new PropertyValueFactory<>("value"));
    }

    private void editableColumns() {
        colValor.setCellFactory(TextFieldTableCell.forTableColumn());
        colValor.setOnEditCommit(event -> {

            String columnName = event.getTableView().getItems().get(event.getTablePosition().getRow()).getColumn();
            valuesObservable.stream().filter(columnValueData -> columnValueData.getColumn().equals(columnName))
                    .findFirst().ifPresent(columnValueData -> {

                if(StringUtils.isBlank(event.getNewValue())){
                    insertMessageLabel.setText(String.format("O valor da coluna %s é obrigatório.", columnName.toUpperCase()));
                }else{

                    columnValueData.setValue(event.getNewValue());
                    btnSave.setDisable(false);
                }
            });

        });
    }


    private Boolean checkAllRequiredFields(){

        List<ColumnValueData> items = valuesObservable.stream()
                .filter(columnValueData -> columnValueData.getRequired() &&
                        StringUtils.isBlank(columnValueData.getValue()))
                .collect(Collectors.toList());

        return !items.isEmpty();
    }

    private void handleBtnSave(ActionEvent e){


        if(checkAllRequiredFields()){
            insertMessageLabel.setText("Preencha os valores de todas as colunas obrigatórias");
            return;
        }else{
            insertMessageLabel.setText("");
        }

        List<ColumnValueData> queryColumnValueData = valuesObservable.stream()
                .map(ColumnValueData::copy)
                .collect(Collectors.toList());

        Query<ColumnValueData> query = new Query<>(table.getSchema(), table.getName(), QueryType.INSERT, queryColumnValueData);

        queriesListView.getItems().add(query);
        valuesObservable.clear();
        valuesTableView.getItems().clear();
        btnSave.setDisable(true);
        this.loadDataToTable();
    }


    public void loadDataToTable(){

        valuesObservable = FXCollections.observableArrayList(table.getColuns().stream()
                .map(column -> new ColumnValueData(false, column.getName(), column.getDataType(), column.getRequired()))
                .collect(Collectors.toList()));
        valuesTableView.setItems(valuesObservable);
    }

    public void setQueriesListView(ListView<Query> queriesListView) {
        this.queriesListView = queriesListView;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    private class CustomColumnCell extends TableCell<ColumnValueData, String> {


        private final Logger log = LoggerFactory.getLogger(CustomColumnCell.class);

        @FXML
        private HBox rowPane;

        @FXML
        private Label name;

        @FXML
        private Label required;

        private FXMLLoader mLLoader;

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if(empty || item == null) {
                setText(null);
                setGraphic(null);
            } else {
                if (mLLoader == null) {
                    mLLoader = new FXMLLoader(getClass().getResource("/views/cells/columnCell.fxml"));
                    mLLoader.setController(this);

                    try {
                        mLLoader.load();
                    } catch (IOException e) {
                        log.error("", e);
                    }
                }

                name.setText(item.split("#")[0]);
                Boolean isRequired = Boolean.valueOf(item.split("#")[1]);
                required.setVisible(isRequired);
                setText(null);
                setGraphic(rowPane);
            }
        }
    }
}
