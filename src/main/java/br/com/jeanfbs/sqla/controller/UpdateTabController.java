package br.com.jeanfbs.sqla.controller;

import br.com.jeanfbs.sqla.enums.QueryType;
import br.com.jeanfbs.sqla.vo.ColumnValueData;
import br.com.jeanfbs.sqla.vo.Query;
import br.com.jeanfbs.sqla.vo.Table;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

public class UpdateTabController {


    private TableView<ColumnValueData> valuesTableView;

    private ObservableList<ColumnValueData> valuesObservable;

    private TableColumn<ColumnValueData, Boolean> colCheckBox;

    private TableColumn<ColumnValueData, String> colColuna;

    private TableColumn<ColumnValueData, String> colTipoDado;

    private TableColumn<ColumnValueData, String> colValor;

    private Button btnSave;

    private Table table;

    private ListView<Query> queriesListView;

    public UpdateTabController(TableView<ColumnValueData> valuesTableView,
                               Button btnSave) {

        this.valuesTableView = valuesTableView;
        this.valuesObservable = valuesTableView.getItems();
        this.colCheckBox = (TableColumn<ColumnValueData, Boolean>)valuesTableView.getColumns().get(0);
        this.colColuna = (TableColumn<ColumnValueData, String>)valuesTableView.getColumns().get(1);
        this.colTipoDado = (TableColumn<ColumnValueData, String>)valuesTableView.getColumns().get(2);
        this.colValor = (TableColumn<ColumnValueData, String>)valuesTableView.getColumns().get(3);
        this.btnSave = btnSave;

        this.initColumns();
        this.editableColumns();

        btnSave.setOnAction(this::handleBtnSave);
        btnSave.setDisable(true);
    }


    private void initColumns(){
        colCheckBox.setCellFactory(CheckBoxTableCell.forTableColumn(colCheckBox));
        colCheckBox.setCellValueFactory(param -> {
            ColumnValueData value = param.getValue();
            SimpleBooleanProperty booleanProperty = new SimpleBooleanProperty(value.getIsWhere());
            booleanProperty.addListener((observable, oldValue, newValue) -> value.setIsWhere(newValue));
            return booleanProperty;
        });

        colColuna.setCellValueFactory(new PropertyValueFactory<>("column"));
        colTipoDado.setCellValueFactory(new PropertyValueFactory<>("type"));
        colValor.setCellValueFactory(new PropertyValueFactory<>("value"));
    }

    private void editableColumns() {

        colValor.setCellFactory(TextFieldTableCell.forTableColumn());
        colValor.setOnEditCommit(event -> {

            if(StringUtils.isBlank(event.getNewValue())){
                return;
            }
            btnSave.setDisable(false);
            String columnName = event.getTableView().getItems().get(event.getTablePosition().getRow()).getColumn();
            valuesObservable.stream().filter(columnValueData -> columnValueData.getColumn().equals(columnName))
                    .findFirst().ifPresent(columnValueData -> {
                columnValueData.setValue(event.getNewValue());
            });
        });
    }

    private void handleBtnSave(ActionEvent e){
        List<ColumnValueData> queryColumnValueData = valuesObservable.stream()
                .map(ColumnValueData::copy)
                .collect(Collectors.toList());

        Query<ColumnValueData> query = new Query<>(table.getSchema(), table.getName(), QueryType.INSERT, queryColumnValueData);
        queriesListView.getItems().add(query);
        valuesObservable.clear();
        valuesTableView.getItems().clear();
        btnSave.setDisable(true);
        this.loadDataToTable();
    }


    public void loadDataToTable(){

        valuesObservable = FXCollections.observableArrayList(table.getColuns().stream()
                .map(column -> new ColumnValueData(false, column.getName(), column.getDataType(), column.getRequired()))
                .collect(Collectors.toList()));
        valuesTableView.setItems(valuesObservable);
    }

    public void setQueriesListView(ListView<Query> queriesListView) {
        this.queriesListView = queriesListView;
    }

    public void setTable(Table table) {
        this.table = table;
    }
}
