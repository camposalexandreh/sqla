package br.com.jeanfbs.sqla.controller;

import br.com.jeanfbs.sqla.vo.ColumnValueData;
import br.com.jeanfbs.sqla.vo.Query;
import br.com.jeanfbs.sqla.vo.Table;
import javafx.application.Platform;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class TableTabController  {

    private static final Logger log = LoggerFactory.getLogger(TableTabController.class);

    @FXML
    private AnchorPane rootTab;

    @FXML
    private Button editQuery;

    @FXML
    private Button deleteQuery;

    @FXML
    private Button buildScript;

    @FXML
    private ListView<Query> queriesListView;

    @FXML
    private TabPane queryTab;

    @FXML
    private TableView<ColumnValueData> insertValuesTableView;

    @FXML
    private Button btnSaveInsert;

    @FXML
    private Label insertMessageLabel;

    private InsertTabController insertTabController;

    @FXML
    private TableView<ColumnValueData> updateValuesTableView;

    @FXML
    private Button btnSaveUpdate;

    private UpdateTabController updateTabController;

    @FXML
    public void initialize() {

        queriesListView.setCellFactory(param -> new QueryCell());
        queryTab.getSelectionModel().selectedItemProperty().addListener(this::handleTabSelectionChanged);

        queriesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        queriesListView.addEventHandler(MouseEvent.MOUSE_CLICKED, this::handleClickQueriesList);
        queriesListView.getItems().addListener((ListChangeListener<? super Query>) c -> {
            boolean isEmpty = queriesListView.getItems().isEmpty();
            buildScript.setDisable(isEmpty);
        });

        deleteQuery.setOnAction(this::handleDeleteQuery);

        instanciateInsertTabController();
        instanciateUpdateTabController();
    }

    private void instanciateInsertTabController() {
        insertTabController = new InsertTabController(insertValuesTableView, insertMessageLabel, btnSaveInsert);
        insertTabController.setQueriesListView(queriesListView);

        Platform.runLater(() -> {
            if(rootTab.getUserData() instanceof Table) {
                Table table = (Table) rootTab.getUserData();
                insertTabController.setTable(table);
            }
            insertTabController.loadDataToTable();
        });
    }

    private void instanciateUpdateTabController() {
        updateTabController = new UpdateTabController(updateValuesTableView,  btnSaveUpdate);
        updateTabController.setQueriesListView(queriesListView);

        Platform.runLater(() -> {
            if(rootTab.getUserData() instanceof Table) {
                Table table = (Table) rootTab.getUserData();
                updateTabController.setTable(table);
            }
            updateTabController.loadDataToTable();
        });
    }

    private void handleDeleteQuery(ActionEvent ev) {

        ObservableList<Integer> selectedIndices = queriesListView.getSelectionModel().getSelectedIndices();
        ObservableList<Query> items = queriesListView.getItems();
        List<Query> filteredItems = IntStream.range(0, items.size()).filter(i -> !selectedIndices.contains(i))
                .mapToObj(items::get).collect(Collectors.toList());
        queriesListView.setItems(FXCollections.observableArrayList(filteredItems));
    }


    private void handleTabSelectionChanged(Observable observable) {
        System.out.println("mudou!");
    }




    private void showAlert(Query query){

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Detalhes da Query");
        alert.setHeaderText(null);
        alert.setContentText(query.asString());

        alert.showAndWait();
    }

    private void handleClickQueriesList(MouseEvent ev) {

        if(ev.getClickCount() == 2){
            Optional<Query> optQuery = queriesListView.getSelectionModel().getSelectedItems().stream().findFirst();
            if(optQuery.isPresent()){
                Query query = optQuery.get();
                showAlert(query);
            }
        }

        if(ev.getClickCount() == 1){
            boolean isEmpty = queriesListView.getItems().isEmpty();
            editQuery.setDisable(isEmpty);
            deleteQuery.setDisable(isEmpty);

        }
    }

    class QueryCell extends ListCell<Query> {


        private final Logger log = LoggerFactory.getLogger(QueryCell.class);

        @FXML
        private ImageView icon;

        @FXML
        private Label description;

        @FXML
        private Tooltip tooltip;

        @FXML
        private HBox rowPane;

        private FXMLLoader mLLoader;


        @Override
        protected void updateItem(Query attrs, boolean empty) {
            super.updateItem(attrs, empty);

            if(empty || attrs == null) {

                setText(null);
                setGraphic(null);

            } else {
                if (mLLoader == null) {
                    mLLoader = new FXMLLoader(getClass().getResource("/views/cells/queryCell.fxml"));
                    mLLoader.setController(this);

                    try {
                        mLLoader.load();
                    } catch (IOException e) {
                        log.error("", e);
                    }
                }

                icon.setImage(resolveIcon(attrs));
                String text;
                if(attrs.getName().isPresent()){
                    text = String.format("%s [ %s.%s ]", attrs.getName().get().toString(), attrs.getSchema().toUpperCase(),
                            attrs.getTableName().toUpperCase());

                }else{
                    text = String.format("%s.%s", attrs.getSchema().toUpperCase(),
                            attrs.getTableName().toUpperCase());
                }

                tooltip.setText(attrs.getType().name().toUpperCase());

                description.setText(text);
                setText(null);
                setGraphic(rowPane);
            }
        }

        private Image resolveIcon(Query attrs){
            switch (attrs.getType()){
                case UPDATE:
                    return new Image(getClass().getResourceAsStream("/assets/icons/update.png"),
                            15, 15, true, true);
                case DELETE:
                    return new Image(getClass().getResourceAsStream("/assets/icons/delete.png"),
                            15, 15, true, true);
                case GRANT:
                    return new Image(getClass().getResourceAsStream("/assets/icons/delete.png"),
                            15, 15, true, true);
                case CREATE:
                    return new Image(getClass().getResourceAsStream("/assets/icons/insert.png"),
                            15, 15, true, true);
                case DROP:
                    return new Image(getClass().getResourceAsStream("/assets/icons/insert.png"),
                            15, 15, true, true);
                default:
                    return new Image(getClass().getResourceAsStream("/assets/icons/insert.png"),
                            15, 15, true, true);
            }
        }
    }

}
