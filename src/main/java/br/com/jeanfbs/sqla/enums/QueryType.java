package br.com.jeanfbs.sqla.enums;

public enum QueryType {

    INSERT,
    UPDATE,
    DELETE,
    GRANT,
    CREATE,
    DROP,
    SELECT
}
