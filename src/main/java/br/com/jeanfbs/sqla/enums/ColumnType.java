package br.com.jeanfbs.sqla.enums;

public enum ColumnType {

    VARCHAR,
    FLOAT,
    DECIMAL,
    BOOLEAN
}
