package br.com.jeanfbs.sqla.components.alerts;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

public class ExceptionAlert {


    private Alert alert;
    private TextArea textArea;

    public ExceptionAlert() {

        this.alert = new Alert(Alert.AlertType.ERROR);
        Label label = new Label("Stacktrace exception:");

        textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        alert.getDialogPane().setExpandableContent(expContent);

    }

    public void setException(Exception ex){

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();
        textArea.setText(exceptionText);
    }

    public void setHeaderText(String headerText) {
        alert.setHeaderText(headerText);
    }

    public void setTitle(String title) {
        alert.setTitle(title);
    }

    public Optional<ButtonType> showAndWait() {
        return alert.showAndWait();
    }
}
