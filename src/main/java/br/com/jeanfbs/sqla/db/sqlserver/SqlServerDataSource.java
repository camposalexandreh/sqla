package br.com.jeanfbs.sqla.db.sqlserver;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;

import java.sql.Connection;

public final class SqlServerDataSource {

    private static SQLServerDataSource ds = null;

    private SqlServerDataSource() {
        // do nothing
    }

    public static Connection getConnection(final SQLConnectionAttributes attrs) throws SQLServerException {

        if(ds == null){

            ds = new SQLServerDataSource();

            ds.setServerName(attrs.getHostName());
            ds.setUser(attrs.getUsername());
            ds.setPassword(attrs.getPassword());

            ds.setPortNumber(attrs.getPort());
            ds.setDatabaseName(attrs.getDataBase());

            ds.setLoginTimeout(10);
        }

        return ds.getConnection();

    }
}
