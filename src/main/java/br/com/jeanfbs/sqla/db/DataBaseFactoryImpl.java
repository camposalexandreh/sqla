package br.com.jeanfbs.sqla.db;

import br.com.jeanfbs.sqla.db.sqlserver.SQLConnectionAttributes;
import br.com.jeanfbs.sqla.db.sqlserver.SqlServerDataSource;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import org.springframework.stereotype.Component;

import java.sql.Connection;

@Component
public class DataBaseFactoryImpl implements DataBaseFactory {


    private Connection connection;

    @Override
    public Connection getInstance(final DriverTypeEnum driverTypeEnum,
                                  final SQLConnectionAttributes attrs) throws SQLServerException {

        switch (driverTypeEnum){
            case ORACLE:
                connection = createConnectionOracleDriver(attrs);
            case SQL_SERVER:
                return createConnectionSqlServerDriver(attrs);
            case SYBASE:
                return createConnectionSybaseDriver(attrs);
            default:
                return createConnectionOracleDriver(attrs);
        }
    }


    private Connection createConnectionOracleDriver(final SQLConnectionAttributes attrs) throws SQLServerException{
        return null;
    }

    private Connection createConnectionSqlServerDriver(final SQLConnectionAttributes attrs) throws SQLServerException {
        return SqlServerDataSource.getConnection(attrs);
    }

    private Connection createConnectionSybaseDriver(final SQLConnectionAttributes attrs) throws SQLServerException{
        return null;
    }
}
