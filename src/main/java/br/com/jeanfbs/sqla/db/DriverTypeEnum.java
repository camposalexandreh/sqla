package br.com.jeanfbs.sqla.db;

public enum DriverTypeEnum {

    ORACLE,
    SQL_SERVER,
    SYBASE
}
