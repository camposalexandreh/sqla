package br.com.jeanfbs.sqla.vo;

import br.com.jeanfbs.sqla.enums.ColumnType;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Column{

    private String name;
    private ColumnType dataType;
    private Boolean required;
}
