package br.com.jeanfbs.sqla.vo;

import br.com.jeanfbs.sqla.enums.ColumnType;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ColumnValueData {

    @NonNull private Boolean isWhere;
    @NonNull private String column;
    @NonNull private ColumnType type;
    @NonNull private Boolean required;
    private String value;

    public ColumnValueData copy(){
        ColumnValueData copy = new ColumnValueData(getIsWhere(), getColumn(), getType(), getRequired());
        copy.setValue(getValue());
        return copy;
    }

}
