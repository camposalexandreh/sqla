package br.com.jeanfbs.sqla.vo;

import lombok.Data;

import java.util.List;


@Data
public class Table {

    private String schema;
    private String name;
    private List<Column> coluns;
}
