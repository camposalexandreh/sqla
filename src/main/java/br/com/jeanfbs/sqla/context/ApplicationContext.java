package br.com.jeanfbs.sqla.context;

import java.sql.Connection;

public final class ApplicationContext {

    private static Connection currentConnection;

    public static void setCurrentConnection(Connection connection) {
        currentConnection = connection;
    }

    public static Connection getConnection() {
        return currentConnection;
    }
}
